((window, undefined) => {
  'use strict';

  window.setHash = function(length) {
    return Math.floor(Math.random() * Math.pow(36,length)).toString(36);
  }

})(window);
