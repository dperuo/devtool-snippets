((undefined) => {
  'use strict';

  window.onscroll = getBoundingClientRectFn;

  const box = document.querySelector('#lga');

  function getBoundingClientRectFn() {

    const vh = window.innerHeight;

    if (box.getBoundingClientRect().top > 0) {
      box.style.background = 'magenta';
    }

    if (box.getBoundingClientRect().top < 0) {
      box.style.background = 'green';
    }

    if (box.getBoundingClientRect().bottom > vh) {
      box.style.background = 'yellow';
    }
  }

})();
