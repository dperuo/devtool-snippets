((undefined) => {
  'use strict';

  const numbers = [];

  for (var i = 11; i--;) {
    const t0 = performance.now();
    console.log(Math.random());
    const t1 = performance.now();
    numbers.push(t1 - t0);
  };

  console.log(numbers.sort()[6].toFixed(4));

})();
