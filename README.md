# devtool-snippets

Cool JavaScript code snippets I use in Chrome Dev Tools

## Architecture

| Location | Description
| :--- | :---
| `*.js` | Code snippets
| `CONTRIBUTING.md` | How to contribute to this project
| `README.md` | This file
